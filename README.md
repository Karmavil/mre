# About

Minimal workable reproducible example to showcase an issue that occurs  when using `glib-compile-resources`.

## Description

When the generated classes (generated form the gresource.xml file that describes the bundle) are integrated in the project. An attept to use them results in a **_Resource does not exist_** error:

```
ExampleApplication::on_activate(): The resource at “/org/gtkmm/exampleapp/window.ui” does not exist
```

According to the gtkmm and gtk documentation this should be a standard procedure for declaration and use of the resources highlighting advantages such as portability.

So far I've been using resources through 

```
auto refBuilder = Gtk::Builder::create();
refBuilder->add_from_file("path/to/resource");
```

without issues. The problem is clearly with the use or declaration of resources through `glib-compile-resources` 

## Dependencies

 - `cmake` >= 3.25.1 (It should work with versions grather than 3.13 but it may occur some warning about policies)

 - `make` >= 4.3 

 - `gtkmm` >= 4.8.x (I've got the same error using 4.11.3)

 - clang-format (optional) - Removed from rule _all_ to avoid the dependenc.

> Please note that it is important you read the Makefile.

It is not that your IDE will build the project and run it for you right out of the box, there are usually extra task to do as in this case.

All you have to do to build de project ir run `make` (just that) from the project directory.

Don't expect to see a widget, it is not going to happend, the current error is listed in the #Description
