#include "exampleappwindow.h"
#include "exampleapp.h"
#include <stdexcept>

ExampleAppWindow::ExampleAppWindow (
    BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &refBuilder)
    : Gtk::ApplicationWindow (cobject), m_refBuilder (refBuilder)
{
}

// static
ExampleAppWindow *
ExampleAppWindow::create ()
{
  // Load the Builder file and instantiate its widgets.
  std::string respath = "/org/gtkmm/exampleapp/window.ui";
  auto refBuilder = Gtk::Builder::create_from_resource (respath);

  auto window = Gtk::Builder::get_widget_derived<ExampleAppWindow> (
      refBuilder, "app_window");
  if (!window)
    throw std::runtime_error ("No \"app_window\" object in window.ui");

  return window;
}

void
ExampleAppWindow::open_file_view (const Glib::RefPtr<Gio::File> & /* file */)
{
}
