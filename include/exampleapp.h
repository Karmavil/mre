#ifndef __RESOURCE_exampleapp_H__
#define __RESOURCE_exampleapp_H__

#include <gio/gio.h>

extern GResource *exampleapp_get_resource (void);
#endif
