#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
BUILD_TYPE=Debug
JOBS=-j4
#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: all
all: build compile run

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: clean
clean: 
	@rm -rf build

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: format
format:
	@clang-format -i `find include/ -type f -name *.hh`
	@clang-format -i `find src/ -type f -name *.cc`

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: generate-resources
generate-resources:
	@${VIRTUAL_ENV}/bin/glib-compile-resources \
		resources/exampleapp.gresource.xml \
		--sourcedir=resources --generate-header \
		&& mv resources/exampleapp.h include
	@${VIRTUAL_ENV}/bin/glib-compile-resources \
		resources/exampleapp.gresource.xml \
		--sourcedir=resources --generate-source \
		&& mv resources/exampleapp.c src

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: update-sources-list
update-sources-list: generate-resources
	@SOURCES=`find src/ -type f -regex .+\.cc?` \
		&& sed -i -E -e \
			"s%set\((SOURCES)[^)]*\)%set(\1 `echo $$SOURCES`)%" \
			CMakeLists.txt

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: build
build: update-sources-list
	@mkdir -p build && \
		cd build && \
		cmake -DCMAKE_BUILD_TYPE:STRING=${BUILD_TYPE} ..

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: rebuild
rebuild: clean all
	@echo "Done"

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: compile
compile:
	@mkdir -p build && cd build && make ${JOBS}

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: run
run:
	@./build/bin/mre

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I
.PHONY: debug
debug:
	gdb build/bin/mre

#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I#I

